package com.webview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

public class BrowserActivity extends AppCompatActivity {
    // Web view client listener
    private final WebViewClient webViewClientListener = new WebViewClient() {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            WebBackForwardList mWebBackForwardList = browser.copyBackForwardList();

            if (mWebBackForwardList.getSize() > 0 && mWebBackForwardList.getCurrentIndex() > 0)
                ((ImageView) findViewById(R.id.backIV)).setImageResource(R.drawable.ic_keyboard_arrow_left_24dp);
            else
                ((ImageView) findViewById(R.id.backIV)).setImageResource(R.drawable.ic_keyboard_arrow_left_24dp_deacvitaed);

            if (mWebBackForwardList.getSize() > 0 && mWebBackForwardList.getSize() - 1 != mWebBackForwardList.getCurrentIndex())
                ((ImageView) findViewById(R.id.nextTV)).setImageResource(R.drawable.ic_keyboard_arrow_right_24dp);
            else
                ((ImageView) findViewById(R.id.nextTV)).setImageResource(R.drawable.ic_keyboard_arrow_right_24dp_deactivited);

            if (mWebBackForwardList.getCurrentIndex() > 0 && mWebBackForwardList.getCurrentIndex() < mWebBackForwardList.getSize() - 1) {
                ((ImageView) findViewById(R.id.nextTV)).setImageResource(R.drawable.ic_keyboard_arrow_left_24dp);
                ((ImageView) findViewById(R.id.backIV)).setImageResource(R.drawable.ic_keyboard_arrow_right_24dp);
            }
            ((TextView) findViewById(R.id.titleTV)).setText(browser.getTitle());
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            shareBody = url;

            return false;
        }

    };
    // end Web view client listener

    // region Member Variables
    private WebView browser;
    private String shareBody;
    // end region Member Variables

    //abstract methods
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.browser_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Don't auto-show the keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        browser = (WebView) findViewById(R.id.dashWebview);
        browser.setWebChromeClient(new WebChromeClient());
        browser.setWebViewClient(webViewClientListener);
        browser.loadUrl("https://www.google.co.in/");

        findViewById(R.id.doneTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.backIV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                browser.goBack();
            }
        });
        findViewById(R.id.nextTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                browser.goForward();
            }
        });

        findViewById(R.id.reloadTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                browser.reload();
            }
        });

        findViewById(R.id.shareTV).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, ""));
            }
        });
    }
    //end abstract methods
}
